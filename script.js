var app = angular.module("app",[]);

app.controller('PruebaController',function($scope) {
	$scope.count = 0;

	$scope.sum = function(){
		$scope.count++;
	}
});

app.config(function() {
  console.log("AQUI");
});

function Rectangulo(){
	this.ancho = 0;
	this.alto = 0;

	this.setAncho = function(ancho){
		this.ancho = ancho;
	}

	this.setAlto = function(alto){
		this.alto = alto;
	}

	this.getArea = function(){
		return this.ancho * this.alto;
	}
}

app.service("rectangulo",Rectangulo);

app.controller('SeguroController', function($scope,$log,$http,$timeout, rectangulo) {
	rectangulo.setAncho(2);
	rectangulo.setAlto(3);

	$scope.area = rectangulo.getArea();

	$scope.count = 0;

	$scope.seguro = {
		nombre:"",
		edad:0,
		sexo:"",
		coberturas:{
			oftalmologia:false,
			dental:false
		},
		enfermedades:{
			corazon:false,
			estomacal:false,
			alergia:false,
			nombreAlergia:""
		}
	}

	//http://54.84.217.123/ecommerce_sandBox/oauth/token?password=toGpli&username=testbsd&grant_type=password&scope=read%20write&client_secret=oiRso3tr&client_id=testbsdapp

	$scope.info;

	$log.debug("Acabamos de crear el $scope");

	var api_url = "http://imdb.wemakesites.net/api/search?q=harry potter";

	var response = $http.get('https://api.q-bitsapp.com/sandbox/ecommerce/v1/oauth/token?password=toGpli&username=testbsd&grant_type=password&scope=read%20write&client_secret=oiRso3tr&client_id=testbsdapp', {
    headers: {'Authorization': 'Basic dGVzdGJzZGFwcDpvaVJzbzN0cg==', 'X-API-KEY': '8dxUxevrhL2Ui0xUzSV7K5Apem2RCRyuqaS0uXsv'}
	}).then(function(data){
		console.log(data.data);
		$scope.info = data.data;
	},function(error){
		console.log("data1");
	});

	$scope.saveSeguro = function(){
		console.log($scope.seguro.enfermedades.corazon);
	}

	$timeout(function() {
		$scope.count++;
	},1000);

});	
